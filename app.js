/**
 * scoring algorithm: 100*x^2
 */

function RowControls() {
    this.playArea = document.querySelector('#game-grid .borders');
    this.rows     = this.playArea.querySelectorAll('.row');
}

function addRows() {
    var playArea = document.querySelector('#game-grid .borders');
    var rows     = playArea.querySelectorAll('.row');

    var emptyRow = document.createElement('div');
        emptyRow.classList.add('row');
    var emptyBrick = document.createElement('div');
        emptyBrick.classList.add('background');
        emptyBrick.classList.add('empty');
    for (var i = 0; i < 10; i++) {
        var emptyBrickClone = emptyBrick.cloneNode();
        emptyRow.appendChild(emptyBrickClone);
    };

    while (rows.length < 22) {
        var emptyRowClone = emptyRow.cloneNode(1);
        playArea.insertBefore(emptyRowClone, rows[0]);
        rows = playArea.querySelectorAll('.row');
    }
}

RowControls.prototype.clearAllRows = function() {
    for (var i = this.rows.length - 1; i >= 0; i--) {
        this.rows[i].remove();
    };
}

// Tetronimo Shapes: t j l s z i o
/**
 * Destributes tetronimoes
 * @param [string] style Should be either "random" or "7bag"
 */
function Tetronimoes(style) {
    this.style = style || "random";
    this.activeBrick = null;
}

Tetronimoes.prototype.fillBag = function() {
    this.bag = ["i", "j", "l", "o", "s", "t", "z"];
}

Tetronimoes.prototype.pickBrick = function() {
    if (!this.bag || this.bag.length === 0) {
        this.fillBag();
    }

    if (this.style === "random") {
        return this.bag[Math.floor(Math.random()*this.bag.length)];
    } else if (this.style === "7bag") {
        return this.bag.splice(Math.floor(Math.random()*this.bag.length), 1)[0];
    }
}

Tetronimoes.prototype.makeBrick = function(letter) {
    var letter = letter || this.pickBrick();
    var brick;
    var parser = new DOMParser();
    switch (letter) {
        case "i":
            brick = parser.parseFromString('<div class="tetromino shape-i cyan" data-color="cyan"><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div></div>', "text/html");
            break;
        case "j":
            brick = parser.parseFromString('<div class="tetromino shape-j blue" data-color="blue"><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div><div class="brick empty"></div><div class="brick empty"></div><div class="brick filled"></div></div>', "text/html");
            break;
        case "l":
            brick = parser.parseFromString('<div class="tetromino shape-l orange" data-color="orange"><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div><div class="brick empty"></div><div class="brick empty"></div></div>', "text/html");
            break;
        case "o":
            brick = parser.parseFromString('<div class="tetromino shape-o yellow" data-color="yellow"><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div></div>', "text/html");
            break;
        case "s":
            brick = parser.parseFromString('<div class="tetromino shape-s lime" data-color="lime"><div class="brick empty"></div><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div><div class="brick empty"></div></div>', "text/html");
            break;
        case "t":
            brick = parser.parseFromString('<div class="tetromino shape-t purple" data-color="purple"><div class="brick filled"></div><div class="brick filled"></div><div class="brick filled"></div><div class="brick empty"></div><div class="brick filled"></div><div class="brick empty"></div></div>', "text/html");
            break;
        case "z":
            brick = parser.parseFromString('<div class="tetromino shape-z red" data-color="red"><div class="brick filled"></div><div class="brick filled"></div><div class="brick empty"></div><div class="brick empty"></div><div class="brick filled"></div><div class="brick filled"></div></div>', "text/html");
            break;
    }

    this.activeBrick = brick.querySelector(".tetromino");
    this.activeBrick.style.left = "160px";
    this.activeBrick.style.bottom = "800px";
    this.activeBrick.style.top = "auto";
    return this.activeBrick;
}

Tetronimoes.prototype.insertBrick = function(el) {
    el.appendChild(this.activeBrick);
}

Tetronimoes.prototype.timedLowering = function(el) {
    var level = 1;
}

Tetronimoes.prototype.drop = function(el) {
    var bottomSide = el.style.bottom || 800;
    el.style.top = "auto";
    if (hasntHitBottom(el) && checkNextRow(el)) {
        el.style.bottom = parseInt(bottomSide) - 40 + "px";
    } else {
        cycleGame();
    }
}

Tetronimoes.prototype.moveLeft = function(el) {
    if (getCoordinates(el).left === 0) {
        return;
    }
    var leftSide = el.style.left || 0;
    el.style.left = parseInt(leftSide) - 40 + "px";
}

Tetronimoes.prototype.moveRight = function(el) {
    if (getCoordinates(el).right === 0) {
        return;
    }
    var leftSide = el.style.left || 0;
    el.style.left = parseInt(leftSide) + 40 + "px";
}

Tetronimoes.prototype.rotate = function(el) {
    var rotation = el.dataset.rotation;
    var innerBricks = el.querySelectorAll('.brick');

    if (!rotation) {
        rotation = 0;
    }

    rotation = parseInt(rotation) - 90;

    el.dataset.rotation = rotation;
    el.style.transform = "rotate(" + rotation +"deg)";

    for (var i = 0; i < innerBricks.length; i++) {
        innerBricks[i].style.transform = "rotate(" + Math.abs(rotation) +"deg)";
    }

}

addEventListener("keyup", function(event) {
    switch (event.keyIdentifier) {
        case "Up":
            t.rotate(t.activeBrick);
            break;
        case "Left":
            t.moveLeft(t.activeBrick);
            break;
        case "Right":
            t.moveRight(t.activeBrick);
            break;
        case "Down":
            t.drop(t.activeBrick);
            break;
    }
});

function hasntHitBottom (el) {
    return el.getBoundingClientRect().bottom < 800
}

function timedLowering(el, level) {
    var level = level || 1;
    var bottomSide = el.style.bottom || 800;
    el.style.top = "auto";

    if (hasntHitBottom(t.activeBrick) && checkNextRow(el)) {
        el.style.bottom = parseInt(bottomSide) - (level*40) + "px";
        setTimeout(function() {
            requestAnimationFrame(function() {
                timedLowering(el, level);
            });
        }, 1000 / level);
    } else {
        cycleGame();
    }
}

function cycleGame() {
    lockBrick(t.activeBrick);
    clearRows();
    t.makeBrick();
    gameboard.playArea.appendChild(t.makeBrick());
    // timedLowering(t.activeBrick);
}

function lockBrick (el) {
    var innerBricks = el.querySelectorAll('.filled');
    var color = el.dataset.color;

    for (var i = 0; i < innerBricks.length; i++) {
        var innerBrickCoords = getCoordinates(innerBricks[i]);
        var rowToFill = document.querySelectorAll('.borders .row')[innerBrickCoords.top + 2];
        var colToFill = rowToFill.querySelectorAll('.background')[innerBrickCoords.left];
            colToFill.classList.remove("empty");
            colToFill.classList.add("filled");
            colToFill.classList.add(color);
    };

    el.remove();
}

function getRealLoc(el) {
    var playArea = document.querySelector('#game-grid .borders').getBoundingClientRect();
    var location = el.getBoundingClientRect();
    return output = {
        bottom: location.bottom,
        height: location.height,
        left: location.left - playArea.left,
        right: playArea.right - location.right,
        top: location.top,
        width: location.width
    };
}

function getCoordinates(el) {
    var loc = getRealLoc(el);
    return {
        left: loc.left / 40,
        right: loc.right / 40,
        top: loc.top / 40
    };
}

function checkNextRow(el) {
    var innerBricks = el.querySelectorAll('.filled');

    for (var i = 0; i < innerBricks.length; i++) {
        var innerBrickCoords = getCoordinates(innerBricks[i]);
        var rowToCheck = document.querySelectorAll('.borders .row')[innerBrickCoords.top + 3];
        var colToCheck = rowToCheck.querySelectorAll('.background')[innerBrickCoords.left];
        if (colToCheck.classList.contains('filled')) {
            return false;
        }
    };
    return true;
}

function clearRows() {
    var rows = document.querySelectorAll('#game-grid .borders .row');
    var rowsToRemove = [];

    function checkCols(row) {
        var columns = row.querySelectorAll('.background');
        for (var j = 0; j < columns.length; j++) {
            if (columns[j].classList.contains('empty')) {
                return false;
            }
        }
        return true;
    }

    for (var i = 0; i < rows.length; i++) {
        if (checkCols(rows[i])) {
            rowsToRemove.push(rows[i]);
        }
    }

    increaseScore(100*Math.pow(rowsToRemove.length, 2));

    for (var i = rowsToRemove.length - 1; i >= 0; i--) {
        rowsToRemove[i].remove();
    };

    addRows(gameboard.playArea);
}

function increaseScore(amount) {
    var amount = amount || 0;
    var scoreHolder = document.querySelector('#score');
    scoreHolder.textContent = parseInt(scoreHolder.textContent) + amount;
}

gameboard = new RowControls();
addRows();
t = new Tetronimoes("7bag");
gameboard.playArea.appendChild(t.makeBrick());
// timedLowering(t.activeBrick);
